#!/bin/sh -xe

name="$1"
rm -rf $name && mkdir $name
tar -xvzf archives/$name*.tar.gz --strip-components 1 -C $name
